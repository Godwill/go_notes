#### Concurrency _______________________________________________________________________________

	- Goroutines and concurrency are similar to threads but work differently. Go gives you 
	  full support to sharing memeory in goroutines. 
	- One goroutine usually uses 4~5 KB of stack memeory. Therefor thousands of go routines can
	  be run on a single computer. 
	- Use the <go> keyword to create a new goroutine, which is a function at the underlying
	  level. (<main() is a goroutine)
	- ***Don't use shared data to communicate, use communication to share data***

	- Go routine Example Syntax

		go hello(a, b, c)

	EX.1 Goroutine

		package main

		import ( 
			"fmt"
			"runtime"
		)

		func say(s string) {
			for i := 0; i < 5; i++ {
				runtime.Gosched()
				fmt.Println(s)
			}
		}

		func main() {
			go say("world") 	// create a new goroutine
			say("hello")		// current goroutine
		}

	EX.1-A
		- <runtime.Gosched()> means let the CPU execute other goroutines, and come back at 
		  some point. 
		- <GOMAXPROCS> sets the default number of threads to run simultaneously, defined by
		  the number of cores available on the CPU. 
		- To take advantage of parallel processing, you have to call <runtime.GOMAXPROCS(n)
		  to set the number of cores you want to use. if <n<1> it changes nothing. 

***Channels***

	- goroutines run in the same memory address space so you have to maintain synchronization
	  when you want to access shared memory. 
	- A <channel> is like two-way pipeline in Unix shells use <channel> to send or recievef
	  data. 
	- The only data type that can be used in channels is the type <channel> and the keyword
	  <chan>. Be aware that you have to use <make> to create a new <channel> 

	Channel Syntax: 

		ci := make(chan int)
		cs := make(chan string)
		cf := (chan interface{})

	Channel uses the operator < <- > to send or receive data. 

		ch <- v 	// send v to channel ch
		v := <-ch 	// receive data from ch, and assign to v

	EX.2 Channel Example

	package main

	import "fmt"

	func sum(a []int, c chan int) {
		total := 0 
		for _, v := range a {
			total += v
		}
		c <- total 	// send total to c
	}

	func main() {
		a := []int{7, 2, 8, -9, 4, 0}

		c := make(chan int)
		go sum(a[:len(a)/2], c)
		go sum(a[len(a)/2:], c)
		x, y := <-c, <-c  	// receive from c

		fmt.Println(x, y, x+y)
	}

	EX.2-A
		- Sending and receiving data in channels blocks by default, so its much easier to
		  use synchronous goroutines. A block that is a goroutine will not continue when
		  receiving data from an empty channel, until other goroutines send data to this
		  channel. 

***buffered channels***

	- Buffered channels can store more than a single elembent. 

		ch := make(chan bool, 4) 

	  here we create a channel that can store 4 boolean elements. In this channel, we are 
      able to send 4 elements into it without blocking, but the goroutine will be blocked 
	  when you try to send a fifth element and no goroutine receives it. 

	EX.3 Synatx

		ch := make(chan type, n)

		n == 0 ! non-buffer (block)
		n > 0 ! buffer (non-block until n elements in the channel)

	EX.4 

		package main

		import "fmt"

		func main() {
			c := make(chan int, 2) 	// change 2 to 1 will have runtime error, but 3 is fine
			c <- 1
			c <- 2
			fmt.Println(<-c)
			fmt.Println(<-c)
		}

***Range and Close***

	- You can use <range> to operate on buffer channels as in slice and map

	EX.5 range on buffered channels

		package main

		import (
			"fmt"
		)

		func fibonacci(n int, c chan int) {
			x, y := 1, 1
			for i := 0; i < n; i++ {
				c <- x
				x, y = y, x+y
			}
			close(c)
		}

		func main() {
			c := make(chan int, 10)
			go fibonacci(cap(c), c)
			for i := range c {
				fmt.Println(i)
			}
		}

	EX.5-A
		- <for i := range c> will not stop reading data from channel until the channel is
		  closed. We use the keywork <close> to close the channel. It's impossible to send
		  or receive data on a closed channel. 
		- You can use <v, ok := <-ch> to test if a channel is closed. If <ok> returns false
		  it means that there is no data in that channel and it was closed. 
IMPORTANT 
		- Remember to always close chanels in producers and not in consumers, or its very easy
		  to get into panic status
		- You don't have to close channels frequently unless you are sure the channel is
		  completely useless, or you want to exit range loops. 

***Select***

	- You can use the <select> keywork to listen to many channels
	- <select> is blocking by default and it continues to execute only when one of the channels
	  has data to send or receive. If several channels are ready to use at the same time, 
	  select chooses which to execute RANDOMLY. 

	EX.6 Selecting Channels

		package main

		import "fmt"

		func fibonacci(c, quit chan int) {
			x, y := 1, 1
			for {
				select {
				case c <- x:
					x, y = y, x+y
				case <- quit:
					fmt.Println("quit")
					return
				}
			}
		}

		func main() {
			c := make(chan int) 
			quit := make(chan int)
			go func() {
				for i := 0; i < 10; i++ {
					fmt.Println(<-c)
				}
				quit <- 0
			}()
			fibonacci(c, quit)
		}

	EX.6-A
		- <select> has a <default> case as well, just like <switch>. When all the channels
		  are not ready for use, it exectutes the default case. 

	EX.7

		select {
		case i := <-c:
			// use i
		default: 
			// executes here when c is blocked
		}

***Timeout***

	- You can use a timeout in the select to prevent a goroutine from being blocked. 

	EX.8 Time Method

		func main() {
			c := make(chan int)
			o := make(chan bool)
			go func() {
				for {
					select {
					case v := <- c:
						println(v)
					case <-time.After(5 * time.Second):
						println("timeout")
						o <- true 
						break
					}
				}
			}()
			<- o
		}

***Runtime Goroutine***

	- The package <runtime> has some functions for dealing with goroutines. 

	<runtime.Goexit()> 
		- Exits the current goroutine, but defered functions will be executed as usual

	<runtime.Gosched()> 
		- Lets the scheduler execute other goroutines and comes back at some point

	<runtime.NumCPU() int>
		- Returns the number of CPU cores

	<runtime.NumGoroutine() int>
		- Runs the number of goroutines

	<runtime.GOMAXPROCS(n int) int>
		- sets how many CPU cores you want to use


















































