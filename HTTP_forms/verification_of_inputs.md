#### Verification of Inputs ____________________________________________________________

	- Rule of Thumb don't trust any data coming from client side user forms. You 
	  have to validation all incoming data before you use it. Many websites are
	  affected by this problem, which is simple but crucial. 

	- There are two ways of verifying form data that are in common use. 
		1) Javascript Validation on the front-end. 
		2) Server validation on the back-end. (section overs server validation)

	Github Page: https://github.com/astaxie/beeku

***Required Fields***

	- You can use <len> function to get the length of a field in order to ensure that
	  users have entered something. 

	EX.1 LEN Validation

		if len(r.Form["username"][0]) == 0 {
			// code for empty field
		}

	EX.1-A
		- <r.Form> treats different form element types differently when they're blank. 
		  Empty textboxes, text areas, and file uploads. It returns an empty string. 
		- Radio buttons, and check boxes: it doesn't even create the corresponding
		  items. Instead you will get errors if you try to access it. 
		- Thereform its safer to use: 
				<r.Form.Get()>
		- to get field values since it will always return empty if the value doesn't 
		  exist. 
		- <r.Form.Get()> only works on single values, you need to use <r.Form> to get
		  the map of values.

***Numbers***

	- Verifing number fields must be turned into <int> type, first. 

	EX.2 Standard INT type verification

		getint, err := strconv.Atoi(r.Form.Get("age"))
		if err != nil {
			// error occurs when convert to number, it may not be a number
		}

		// check range of number 
		if getint > 100 {
			// to big error
		}

	EX.3 Regular Expression Checking 

		if m, _ := regexp.MatchString("^[0-9]+$", r.Form.Get("age")); !m {
			return false
		} 

	EX.3-A 
		- Regex isn't good for high preformance purposes, regular expression are not 
		  efficient. 
		- Regex is the only way to verify chinese characters. 

	EX.4 English Letters

		if m, _ := regexp.MatchString("^[a-zA-z]+$", r.Form.Get("engname")); !m {
			return false
		}

	EX.5 Email Address
		
		if m, _ := regexp.MatchString(`^([\w\.\_]{2,10})@(\w{1,}).([a-z]{2,4})$`, r.Form.Get("email")); !m {
			fmt.Println("no")
		} else {
			fmt.Println("yes")
		}

***Drop Down List***

	- Check for fabricated values created by hacker from dropdown values: 

	EX.6 Sanitize our Input 

		slice := []string{"apple", "pear", "banana"}

		for _, v := range slice {
			if v == r.Form.Get("fruit") {
				return true
			}
		}
		return false

***Radio Buttons***

	EX.7 Checking for non-radio send values (1/2) checking for a random value

		slice := []int{1,2}

		for _, v := range slice {
			if v == r.Form.Get("gender") {
				return true
			}
		}
		return false 

***Check Boxes***

	EX.8 Sanitization to validate the button and checkbox inputs

		slice := []string{"football", "basketball", "tennis"}

		a := Slice_diff(r.Form["interest"], slice)
		if a == nil {
			return true
		}
		return false 

***Date and Time***

	EX.9 <time> package for converting year, month, day: verification

		t := time.Date(2009, time.November, 10, 23, 0, 0, 0, time.UTC)
		fmt.Printf("go launched at %s\n", t.Local())































