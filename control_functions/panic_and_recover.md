
#### Panic and Recover Functions _________________________________________________________________

	- Go doesn't have <try-catch> structure like Java. Go uses <panic> and <recover> to deal with 
	  errors. However you shouldn't use <panic> very often. 

***	- <Panic> is a built-in function to break the normal flow of programs and get into panic status. 
	  When a function <F> calls <panic>, <F> will not continue executing but its <defer> functions
	  will continue to execute. Then <F> goes back to the break point which caused the panic status. 
	  The program will not terminate until al of these functions return with panic to the first level
	  of that <goroutine>. <panic> can be produced by calling <panic> in the program, and some errors
	  also cause <panic> like array access out of bounds errors. 

	- <Recover> is a built-in function to recover <goroutines> from panic status. Calling <recover> in 
	  <defer> functions is useful because normal functions will not be executed when the program is in
	  panic status. It catches <panic> values if the program is in the panic status, and it gets <nil> 
	  if the program is not in panic status. 

	EX.1 Panic

		var user = os.Getenv("USER") 

		func init() {
			if user == "" {
				panic("no value for $USER")
			}
		}

	EX.2 How to check Panic status

		func throwsPanic(f func()) (b bool) {
			defer func() {
				if x := recover(); x != nil {
					b = true
				}
			}()
			f() 	// if f causes panic, it will recover 
			return 
		}










































